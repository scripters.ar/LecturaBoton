# LecturaPulsador

En este repositorio encontraras información básica sobre como leer y conectar un pulsador con Arduino a través de comunicación serial.

## Materiales

* **Una placa Arduino**
* **Un Pulsador**
* **Una Resistencia de 10k**

## Uso

```
En la carpeta diagrams encontraras la conexion del circuito.
```

```
En la carpeta src encontraras el codigo fuente de ejemplo.
```

```
Una vez compilado el codigo en el IDE de arduino ve a Herramientas --> Monitor Serial
```

Una vez activado el monitor serial nos mostrara información sobre el pulsador.

## Autor

* **Edermar Dominguez** - [Ederdoski](https://github.com/ederdoski)

## License

This code is open-sourced software licensed under the [MIT license.](https://opensource.org/licenses/MIT)

