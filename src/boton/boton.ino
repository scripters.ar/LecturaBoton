int pulsador = 2;   // seleccionamos pin para el pulsador
int val = 0;        // variable para leer estado del pulsador

void setup() {
  Serial.begin(9600);
  pinMode(pulsador, INPUT);    // configuramos el pulsador como Entrada
}

void loop() {
  val = digitalRead(pulsador);
  Serial.println(val);  // Imprimimos el valor del pulsador por Serial
}

